package com.yiyh.test;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.LocalDateTimeUtil;

import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello Workld!!!");

        LocalDateTime localDateTime = LocalDateTimeUtil.parse("2022-10-28", DatePattern.NORM_DATE_FORMATTER);
        System.out.println(localDateTime);
        LocalDateTime dateTime = localDateTime.plusDays(360);
        System.out.println(dateTime);
    }
}
