package com.yiyh.test;

public class Math {

    private Integer myInt1;
    private Integer myInt2;

    public Math() {
    }

    public Math(Integer myInt1, Integer myInt2) {
        this.myInt1 = myInt1;
        this.myInt2 = myInt2;
    }

    public int sum() {
        return getMyInt1() + getMyInt2();
    }

    public Integer getMyInt1() {
        return myInt1;
    }

    public void setMyInt1(Integer myInt1) {
        this.myInt1 = myInt1;
    }

    public Integer getMyInt2() {
        return myInt2;
    }

    public void setMyInt2(Integer myInt2) {
        this.myInt2 = myInt2;
    }

    public static void main(String[] args) {
        Math myObject = new Math(1, 2);
        int sum = myObject.sum();
        System.out.println(sum);

    }
}
