package com.yiyh.pract;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import org.junit.Test;

import java.util.List;

public class FilterPhoneByColorTest {

    public List<Phone> filteredPhoneListByColor(List<Phone> phoneList, String color) {
        List<Phone> filteredPhones = CollUtil.toList();
        for (Phone phone : phoneList) {
            if (ObjectUtil.equals(color, phone.getColor())) {
                filteredPhones.add(phone);
            }
        }
        return filteredPhones;
    }

    @Test
    public void testColor() {
        List<Phone> phoneList01 = filteredPhoneListByColor(PhoneUtil.getPhoneList(), "深空灰色");
        System.out.println("深空灰色："+phoneList01);

        List<Phone> phoneList02 = filteredPhoneListByColor(PhoneUtil.getPhoneList(), "银色");
        System.out.println("银色："+phoneList02);
    }
}
