package com.yiyh.pract;

import com.alibaba.druid.filter.config.ConfigTools;

public class DesPassword {
    public static void main(String[] args) throws Exception{
        String publickey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMBNZmTAzM78RhYX1iyipxTF3bC1KnvI3BteXeStqW5bqfzUMnl44oDZxEEWItspgkyk057c3E3hQCa6BnnwQfkCAwEAAQ==";
        String password = "acP4nub2d+CSkr0eiJO4VQ3WuQ4Nscp9ViI5+VZzQ9b04EU73UYOU7fbpCzsF2noxNK+qFC8z6EFP9fmJNV3Bw==";
        String pwd = ConfigTools.decrypt(publickey, password);

        System.out.println(pwd);
    }

}
