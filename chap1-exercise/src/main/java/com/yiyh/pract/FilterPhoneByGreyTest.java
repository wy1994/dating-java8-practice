package com.yiyh.pract;

import cn.hutool.core.collection.CollUtil;
import org.junit.Test;

import java.util.List;

/**
 * 我又想看看颜色是深空灰色的手机有哪些？
 */
public class FilterPhoneByGreyTest {

    @Test
    public void filterPhoneByColor(){
        List<Phone> filteredPhones = CollUtil.toList();
        for (Phone phone : PhoneUtil.getPhoneList()) {
            if("深空灰色".equals(phone.getColor())){
                filteredPhones.add(phone);
            }
        }
        System.out.println(filteredPhones);
    }
}
