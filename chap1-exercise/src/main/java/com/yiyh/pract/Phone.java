package com.yiyh.pract;

/**
 * 手机的实体类
 */
public class Phone {

    /**
     * 编号
     */
    private Integer id;

    /**
     * 名称
     */
    private String productName;

    /**
     * 颜色
     */
    private String color;

    /**
     * 种类
     */
    private String spec;

    /**
     * 价格
     */
    private Integer price;

    public Phone() {
    }

    public Phone(Integer id, String productName, String color, String spec, Integer price) {
        this.id = id;
        this.productName = productName;
        this.color = color;
        this.spec = spec;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", color='" + color + '\'' +
                ", spec='" + spec + '\'' +
                ", price=" + price +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
