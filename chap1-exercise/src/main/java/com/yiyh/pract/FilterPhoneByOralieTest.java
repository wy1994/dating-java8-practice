package com.yiyh.pract;

import cn.hutool.core.collection.CollUtil;
import org.junit.Test;

import java.util.List;

/**
 * 我又想看看颜色是金色的手机有哪些？
 */
public class FilterPhoneByOralieTest {

    /**
     * 过滤金色的手机
     */
    @Test
    public void filterPhoneByOralie(){
        List<Phone> filteredPhones = CollUtil.toList();
        for (Phone phone : PhoneUtil.getPhoneList()) {
            if("金色".equals(phone.getColor())){
                filteredPhones.add(phone);
            }
        }
        System.out.println(filteredPhones);
    }
}
