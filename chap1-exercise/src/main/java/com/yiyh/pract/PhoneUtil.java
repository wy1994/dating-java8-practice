package com.yiyh.pract;

import cn.hutool.core.collection.CollUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 手机的工具类
 */
public class PhoneUtil {
    /**
     * 初始化手机列表
     *
     * @return
     */
    public static List<Phone> getPhoneList() {
        Phone phone1 = new Phone(1, "iPhone 11 Pro", "深空灰色", "64GB", 8699);
        Phone phone2 = new Phone(2, "iPhone 11 Pro", "银色", "64GB", 8700);
        return CollUtil.newArrayList(phone1,phone2);
    }
}
