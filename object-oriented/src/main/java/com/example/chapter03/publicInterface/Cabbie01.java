package com.example.chapter03.publicInterface;

public class Cabbie01 {

    private Integer count;

    public Cabbie01() {
        System.out.println("成功调用 Cabbie01() 的构造方法");
        count = 0;
    }

    public int Cabbie01() {
        return 1;
    }

    public Integer getCount() {
        return count;
    }
}
