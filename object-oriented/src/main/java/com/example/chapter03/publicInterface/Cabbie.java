package com.example.chapter03.publicInterface;

public interface Cabbie {



    void hailTaxi();


    void enterTaxi();


    void greetCabbie();


    void specifyDestination();


    void payCabbie();


    void tipCabbie();


    void leaveTaxi();


}
