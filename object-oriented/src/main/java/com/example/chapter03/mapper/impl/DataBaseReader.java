package com.example.chapter03.mapper.impl;

public class DataBaseReader {

    private String dbName;
    private Integer startPosition;
    private Integer expectPosition;

    public DataBaseReader(String dbName) {
        this.dbName = dbName;
        this.startPosition = 0;
    }

    public DataBaseReader(String dbName, Integer startPosition) {
        this.dbName = dbName;
        this.startPosition = startPosition;
    }

    public DataBaseReader(Integer expectPosition, String dbName) {
        this.dbName = dbName;
        this.expectPosition = expectPosition;
    }
}
