package com.example.chapter03.server.objectProperties;

public class Number01 {

    private Integer count;

    public void method1() {
        count = 1;
    }

    public void method2() {
        count = 2;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public static void main(String[] args) {
        Number01 number01 = new Number01();
        Number01 number02 = new Number01();
        Number01 number03 = new Number01();

        number01.setCount(2);
        number02.setCount(3);
        number03.setCount(4);

        System.out.println(number01.getCount());
        System.out.println(number02.getCount());
        System.out.println(number03.getCount());
    }
}
