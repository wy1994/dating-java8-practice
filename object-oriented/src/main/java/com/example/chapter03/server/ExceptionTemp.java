package com.example.chapter03.server;

import org.junit.Test;

public class ExceptionTemp {

    @Test
    public void testExce01() {
        int count = 0;
        try {
            count = 5/count;
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
            count = 1;
        }
        System.out.println("The exception is handled.");
        System.out.println(count);
    }


    @Test
    public void testExce02() {
        int count = 0;
        try {
            count = 5/count;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            count = 1;
        }
        System.out.println("The exception is handled.");
        System.out.println(count);
    }
}
