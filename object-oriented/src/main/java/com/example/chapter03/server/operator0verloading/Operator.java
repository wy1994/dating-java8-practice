package com.example.chapter03.server.operator0verloading;

import javafx.scene.transform.MatrixType;

public class Operator {
    public static void main(String[] args) {
        int X = 5 + 6;
        System.out.println(X);

        String firstName = "Joe", lastName = "Smith";

        String name = firstName + " " + lastName;
        System.out.println(name);

//        Matrix a , b , c;
//        c = a + b;

        X = 5 + (-1 * 6);
        System.out.println(X);
    }
}
