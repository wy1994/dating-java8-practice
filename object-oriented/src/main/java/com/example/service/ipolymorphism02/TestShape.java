package com.example.service.ipolymorphism02;

import org.junit.Test;

import java.util.Stack;

public class TestShape {

    @Test
    public void test01() {

        Shape circle = new Circle(5.0);
        Double circleArea = circle.getArea();
        System.out.println(circleArea);

        Shape rectangle = new Rectangle(8.0, 5.0);
        Double rectangleArea = rectangle.getArea();
        System.out.println(rectangleArea);


    }

    @Test
    public void test02() {
        Shape circle = new Circle(5.0);
        Shape rectangle = new Rectangle(4.0, 5.0);

        Stack<Object> stack = new Stack<>();
        stack.push(circle);
        stack.push(rectangle);

        while (!stack.empty()) {
            Shape shape = (Shape) stack.pop();
            System.out.println("Area = " + shape.getArea());
        }
    }
}
