package com.example.service.ipolymorphism02;


public class Rectangle extends Shape {

    private Double length;
    private Double width;

    public Rectangle() {
    }

    public Rectangle(Double length, Double width) {
        this.length = length;
        this.width = width;
    }


    @Override
    public Double getArea() {
        Double area = length * width;
        return area;
    }
}
