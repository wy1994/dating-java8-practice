package com.example.service.ipolymorphism02;

public abstract class Shape {

   private Double area;

   public abstract Double getArea();
}
