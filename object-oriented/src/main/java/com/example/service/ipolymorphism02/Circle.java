package com.example.service.ipolymorphism02;


public class Circle extends Shape {

    private Double radius;

    public Circle() {
    }

    public Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public Double getArea() {
        Double area = 3.14 * (radius * radius);
        return area;
    }
}
