package com.example.service.publicInterface;

public interface Cabbie {

    void hailTaxi();


    void enterTaxi();


    void greetCabbie();


    void specifyDestination();


    void payCabbie();


    void tipCabbie();


    void leaveTaxi();


}
