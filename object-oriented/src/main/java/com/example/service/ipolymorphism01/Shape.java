package com.example.service.ipolymorphism01;

public abstract class Shape {

   public abstract void draw();
}
