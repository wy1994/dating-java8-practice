package com.example.service.ipolymorphism01;

public class TestShape {

   public static void main(String[] args) {

      Shape circle = new Circle();
      circle.draw();

      Shape square = new Square();
      square.draw();

      Shape star = new Star();
      star.draw();
   }
}
