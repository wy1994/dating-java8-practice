package com.example.service.is_a;

public abstract class Shape {

   public abstract void draw();
}
