package com.example.service.is_a;

public class TestShape {

   public static void main(String[] args) {

      Shape circle = new Circle();
      circle.draw();

      Shape square = new Square();
      square.draw();

      Shape star = new Star();
      star.draw();
   }
}
