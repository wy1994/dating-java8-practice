package com.example.service;

public interface Calculate {
    Integer calculate(Integer value);
}
