package com.example.service;

public class TestIntSquare {
    public static void main(String[] args) {
        Calculate intSquare = new IntSquare();
        Integer intSquareResult = intSquare.calculate(6);
        System.out.println(intSquareResult);

        Calculate intPow = new IntPow();
        Integer intPowResult = intPow.calculate(6);
        System.out.println(intPowResult);
    }
}
