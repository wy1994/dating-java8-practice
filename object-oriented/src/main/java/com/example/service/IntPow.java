package com.example.service;

/**
 * 使用类库法计算平方根
 */
public class IntPow implements Calculate{

    private Integer squareValue;

    private Integer calculateSquare01(Integer value) {
        Double pow = Math.pow(value.doubleValue(), 2);
        return pow.intValue();
    }

    @Override
    public Integer calculate(Integer value) {
        squareValue = calculateSquare01(value);
        return squareValue;
    }
}


