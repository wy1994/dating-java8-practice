package com.example.service;

/**
 * 使用算数法计算平方根
 */
public class IntSquare  implements Calculate{

    private Integer squareValue;

    private Integer calculateSquare(Integer value) {
        return value * value;
    }


    @Override
    public Integer calculate(Integer value) {
        squareValue = calculateSquare(value);
        return squareValue;
    }
}


