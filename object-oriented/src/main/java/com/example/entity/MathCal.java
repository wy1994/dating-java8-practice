package com.example.entity;

public class MathCal {

    private Integer num;


    public MathCal() {
    }

    public MathCal(Integer num) {
        this.num = num;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer calSquare() {
        return num * num;
    }

    public static void main(String[] args) {
        MathCal mathCal = new MathCal();
        mathCal.setNum(4);
        Integer calSquare = mathCal.calSquare();
        System.out.println(calSquare);
    }
}
