package com.example.entity.extend;

public class Mammal {

    private Integer eyeColor;

    public Integer getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(Integer eyeColor) {
        this.eyeColor = eyeColor;
    }
}
