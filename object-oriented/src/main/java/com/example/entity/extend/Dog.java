package com.example.entity.extend;

public class Dog extends Mammal {

    private Integer barkFrequency;

    public Integer getBark() {
        return barkFrequency;
    }

    public void setBark(Integer barkFrequency) {
        this.barkFrequency = barkFrequency;
    }
}
