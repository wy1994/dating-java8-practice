package com.example.entity.extend;

public class Cat extends Mammal {

    private Integer meowFrequency;

    public Integer getMeow() {
        return meowFrequency;
    }

    public void setMeow(Integer meowFrequency) {
        this.meowFrequency = meowFrequency;
    }
}
