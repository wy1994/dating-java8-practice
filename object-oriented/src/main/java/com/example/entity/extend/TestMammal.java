package com.example.entity.extend;

public class TestMammal {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.getEyeColor();
        cat.getMeow();

        Dog dog = new Dog();
        dog.getEyeColor();
        dog.getBark();
    }
}
