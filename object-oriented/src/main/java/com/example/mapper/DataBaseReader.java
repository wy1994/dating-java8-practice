package com.example.mapper;

public interface DataBaseReader {

    void open(String name);

    void close();

    void goToFirst();

    void goToLast();

    Integer howManyRecords();

    Boolean areThereMoreRecords();

    void positionRecord();

    String getRecord();

    String getNextRecord();

}
