package com.example.mapper.impl;

import com.example.mapper.DataBaseReader;

public class DataBaseReaderImpl implements DataBaseReader {


    @Override
    public void open(String name) {
        /* 一些具体的应用程序处理代码 */
//        /* 调用Oracle API来打开数据库 */
        /*调用SQLAnywhere API来打开数据库*/
        /* 一些更多的具体的应用程序处理代码 */
    }

    @Override
    public void close() {

    }

    @Override
    public void goToFirst() {

    }

    @Override
    public void goToLast() {

    }

    @Override
    public Integer howManyRecords() {
        return null;
    }

    @Override
    public Boolean areThereMoreRecords() {
        return null;
    }

    @Override
    public void positionRecord() {

    }

    @Override
    public String getRecord() {
        return null;
    }

    @Override
    public String getNextRecord() {
        return null;
    }
}
