package com.lingyejun.dating.chap1.practice.strategyPattern;


import com.lingyejun.dating.chap1.practice.Phone;

/**
 * 按照价格过滤
 */
public class PhonePricePredicate implements PhonePredicate {

    @Override
    public boolean test(Phone phone) {
        if (phone.getPrice() > 8000) {
            return true;
        }
        return false;
    }
}
