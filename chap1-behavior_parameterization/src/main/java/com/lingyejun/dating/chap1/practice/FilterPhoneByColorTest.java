package com.lingyejun.dating.chap1.practice;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 2.我又想看看颜色是金色的手机有哪些？
 *
 * @author yiyh
 * @date 2021-11-21
 */
public class FilterPhoneByColorTest {

    /**
     * 初始化手机列表
     *
     * @return
     */
    public static List<Phone> initGreyPhoneList() {
        List<Phone> phones = new ArrayList<>();
        Phone phone1 = new Phone(1, "iPhone 11 Pro", "深空灰色", "64GB", 8699);
        Phone phone2 = new Phone(2, "iPhone 11 Pro", "银色", "64GB", 8700);
        phones.add(phone1);
        phones.add(phone2);
        return phones;
    }

    /**
     * 按颜色过滤手机
     *
     * @param phoneList
     * @return
     */
    public List<Phone> filterPhoneByColor(List<Phone> phoneList, String color) {
        List<Phone> filteredPhones = new ArrayList<>();
        for (Phone phone : phoneList) {
            if (Objects.equals(color, phone.getColor())) {
                filteredPhones.add(phone);
            }
        }
        return filteredPhones;
    }

    @Test
    public void filteredGolderPhone() {
        List<Phone> filteredList = initGreyPhoneList();
        List<Phone> phoneList01 = filterPhoneByColor(filteredList, "深空灰色");
        List<Phone> phoneList02 = filterPhoneByColor(filteredList, "银色");
        phoneList01.forEach(System.out::println);
        phoneList02.forEach(System.out::println);
    }

}
