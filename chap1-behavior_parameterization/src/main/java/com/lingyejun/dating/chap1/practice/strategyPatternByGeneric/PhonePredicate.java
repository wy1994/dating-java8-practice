package com.lingyejun.dating.chap1.practice.strategyPatternByGeneric;

import com.lingyejun.dating.chap1.practice.Phone;

/**
 * 判断手机是否满足某一类条件
 */
@FunctionalInterface
public interface PhonePredicate {

    boolean test(Phone phone);
}
