package com.lingyejun.dating.chap1.practice.strategyPatternByGeneric;

import com.lingyejun.dating.chap1.practice.Phone;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * 手机的泛型类
 *
 * @param <T>
 */
public class PhoneMainType<T> {

    /**
     * 初始化手机列表
     *
     * @return
     */
    public static List<Phone> initGreyPhoneList() {
        List<Phone> phones = new ArrayList<>();
        Phone phone1 = new Phone(1, "iPhone 11 Pro", "深空灰色", "64GB", 8699);
        Phone phone2 = new Phone(2, "iPhone 11 Pro", "银色", "64GB", 8700);
        phones.add(phone1);
        phones.add(phone2);
        return phones;
    }

    /**
     * 根据传入的类型判断是否符合需求
     *
     * @param list
     * @param predicate
     * @return
     */
    public List<T> userPredicateImpl(List<T> list, Predicate<T> predicate) {
        List<T> filteredList = new ArrayList<>();
        for (T t : list) {
            if (predicate.test(t)) {
                filteredList.add(t);
            }
        }
        return filteredList;
    }

    /**
     * 测试泛型
     */
    @Test
    public void testPhoneMainType(){
        PhoneMainType<Phone> phoneMainType = new PhoneMainType<>();
        List<Phone> phones = initGreyPhoneList();
        List<Phone>  filteredList = phoneMainType.userPredicateImpl(phones, (Phone phone) -> "64GB".equals(phone.getSpec()));
        System.out.println(filteredList);
    }
}
