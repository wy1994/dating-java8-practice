package com.lingyejun.dating.chap1.practice.strategyPatternByLamd;


import com.lingyejun.dating.chap1.practice.Phone;

/**
 * 按照价格过滤
 */
public class PhonePricePredicate implements PhonePredicate {

    @Override
    public boolean test(Phone phone) {
        return phone.getPrice() > 8000;
    }
}
