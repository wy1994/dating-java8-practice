package com.lingyejun.dating.chap1.practice;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 我想看看颜色是深空灰色的手机有哪些
 *
 * @author yiyh
 * @date 2021-11-21
 */
public class FilterGreyPhoneTest {

    /**
     * 初始化手机列表
     *
     * @return
     */
    public static List<Phone> initGreyPhoneList() {
        List<Phone> phones = new ArrayList<>();
        Phone phone1 = new Phone(1, "iPhone 11 Pro", "深空灰色", "64GB", 8699);
        Phone phone2 = new Phone(2, "iPhone 11 Pro", "银色", "64GB", 8700);
        phones.add(phone1);
        phones.add(phone2);
        return phones;
    }

    /**
     * 1.我想看看颜色是深空灰色的手机有哪些？
     *
     * @param phoneList
     * @return
     */
    public List<Phone> filterGreyPhone(List<Phone> phoneList) {
        List<Phone> filteredPhones = new ArrayList<>();
        for (Phone phone : phoneList) {
            if ("深空灰色".equals(phone.getColor())) {
                filteredPhones.add(phone);
            }
        }
        return filteredPhones;
    }


    @Test
    public void filteredDeepGreyPhone() {
        List<Phone> filteredList = initGreyPhoneList();
        List<Phone> phones1 = filterGreyPhone(filteredList);
        phones1.forEach(System.out::println);
    }

}
