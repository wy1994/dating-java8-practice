package com.lingyejun.dating.chap1.practice.strategyPattern;

import com.lingyejun.dating.chap1.practice.Phone;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 手机过滤测试类
 */
public class PhonePredicateTest {

    /**
     * 初始化手机列表
     *
     * @return
     */
    public static List<Phone> initGreyPhoneList() {
        List<Phone> phones = new ArrayList<>();
        Phone phone1 = new Phone(1, "iPhone 11 Pro", "深空灰色", "64GB", 8699);
        Phone phone2 = new Phone(2, "iPhone 11 Pro", "银色", "64GB", 8700);
        phones.add(phone1);
        phones.add(phone2);
        return phones;
    }

    /**
     * 按照策略进行筛选
     *
     * @param phoneList
     * @param phonePredicate
     * @return
     */
    public List<Phone> userPredicateImpl(List<Phone> phoneList, PhonePredicate phonePredicate) {
        List<Phone> filteredPhones = new ArrayList<>();
        for (Phone phone : phoneList) {
            if (phonePredicate.test(phone)) {
                filteredPhones.add(phone);
            }
        }
        return filteredPhones;
    }

    /**
     * 通过颜色进行过滤
     */
    @Test
    public void filteredByColor() {
        List<Phone> phones = initGreyPhoneList();
        List<Phone> colorPredicate = userPredicateImpl(phones, new PhoneColorPredicate());
        System.out.println(colorPredicate);
    }

    /**
     * 通过价格进行过滤
     */
    @Test
    public void filteredByPrice() {
        List<Phone> phones = initGreyPhoneList();
        List<Phone> pricePredicate = userPredicateImpl(phones, new PhonePricePredicate());
        System.out.println(pricePredicate);
    }

    /**
     * 使用匿名内部类进行过滤
     */
    @Test
    public void filteredByAnonymous() {
        List<Phone> phones = initGreyPhoneList();
        List<Phone> predicateList = userPredicateImpl(phones, new PhonePredicate() {
            @Override
            public boolean test(Phone phone) {
                if ("暗夜绿色".equals(phone.getColor())) {
                    return true;
                }
                return false;
            }
        });
        System.out.println(predicateList);
    }

    /**
     * 使用Lambda表达式进行过滤
     */
    @Test
    public void filterByLambda() {
        List<Phone> phones = initGreyPhoneList();
        List<Phone> predicateList = userPredicateImpl(phones, (Phone phone) -> {
            if ("暗夜绿色".equals(phone.getColor())) {
                return true;
            }
            return false;
        });
        System.out.println(predicateList);
    }
}
