package com.lingyejun.dating.chap1.practice.strategyPatternByGeneric;


import com.lingyejun.dating.chap1.practice.Phone;

/**
 * 按照颜色去筛选
 */
public class PhoneColorPredicate implements PhonePredicate {
    @Override
    public boolean test(Phone phone) {
        return "深空灰色".equals(phone.getColor());
    }
}
