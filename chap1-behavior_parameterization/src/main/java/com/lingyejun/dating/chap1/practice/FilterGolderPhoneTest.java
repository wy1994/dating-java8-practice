package com.lingyejun.dating.chap1.practice;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 2.我又想看看颜色是金色的手机有哪些？
 *
 * @author yiyh
 * @date 2021-11-21
 */
public class FilterGolderPhoneTest {

    /**
     * 初始化手机列表
     *
     * @return
     */
    public static List<Phone> initGreyPhoneList() {
        List<Phone> phones = new ArrayList<>();
        Phone phone1 = new Phone(1, "iPhone 11 Pro", "深空灰色", "64GB", 8699);
        Phone phone2 = new Phone(2, "iPhone 11 Pro", "银色", "64GB", 8700);
        phones.add(phone1);
        phones.add(phone2);
        return phones;
    }

    /**
     * 2.我又想看看颜色是金色的手机有哪些？
     *
     * @param phoneList
     * @return
     */
    public List<Phone> filterGolderPhone(List<Phone> phoneList) {
        List<Phone> filteredPhones = new ArrayList<>();
        for (Phone phone : phoneList) {
            if ("金色".equals(phone.getColor())) {
                filteredPhones.add(phone);
            }
        }
        return filteredPhones;
    }

    @Test
    public void filteredGolderPhone() {
        List<Phone> filteredList = initGreyPhoneList();
        List<Phone> phoneList = filterGolderPhone(filteredList);
        phoneList.forEach(System.out::println);
    }

}
